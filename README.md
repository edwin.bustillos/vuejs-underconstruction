# underconstruction

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Docker Build image 
```
docker build --tag edwinbustillos/vuejs-underconstruction .
```

### Docker Tag image
```
docker image tag edwinbustillos/vuejs-underconstruction:latest
```

### Docker Push image
```
docker image push edwinbustillos/vuejs-underconstruction:latest
```